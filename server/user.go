package main

import (
	"encoding/json"

	"github.com/NeuroPyrox/go-cached-json"
	"github.com/NeuroPyrox/strictjson"
)

type User interface {
	EventStream
	Data
}

type user struct {
	eventStream
	data
}

type newUserFields struct {
	Name   string
	LatLng json.RawMessage
}

func NewUser(uid string, jsonText []byte) (User, error) {
	fields := newUserFields{}
	err := strictjson.UnmarshalStruct(jsonText, &fields)
	if err != nil {
		return nil, err
	}
	latLng, err := NewLatLng(fields.LatLng)
	if err != nil {
		return nil, err
	}
	u := &user{
		data: data{
			dataFields: dataFields{
				UID:    uid,
				Name:   fields.Name,
				LatLng: latLng,
			},
		},
	}
	u.Cache = cachedjson.New(&u.dataFields)
	return u, nil
}
