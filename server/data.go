package main

import (
	"encoding/json"
	"sync"

	"github.com/NeuroPyrox/go-cached-json"
)

type ConstData interface {
	json.Marshaler
	MarshalLatLngJSON() ([]byte, error)
	GetUID() string
	GetConstLatLng() ConstLatLng
}

type Data interface {
	ConstData
	UnmarshalLatLngJSON(jsonText []byte) error
}

type data struct {
	sync.Mutex
	cachedjson.Cache
	dataFields
}

type dataFields struct {
	UID    string
	Name   string
	LatLng LatLng
}

func (d *data) MarshalJSON() ([]byte, error) {
	d.Lock()
	defer d.Unlock()
	return d.Cache.MarshalJSON()
}

func (d *data) MarshalLatLngJSON() ([]byte, error) {
	return d.LatLng.MarshalJSON()
}

func (d *data) UnmarshalLatLngJSON(jsonText []byte) error {
	err := d.LatLng.UnmarshalJSON(jsonText)
	if err == nil {
		d.Lock()
		d.Update()
		d.Unlock()
	}
	return err
}

func (d *data) GetUID() string {
	return d.UID
}

func (d *data) GetConstLatLng() ConstLatLng {
	return d.LatLng
}
