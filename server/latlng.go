package main

import (
	"encoding/json"
	"sync"

	"github.com/NeuroPyrox/go-cached-json"
	"github.com/NeuroPyrox/strictjson"
)

type ConstLatLng interface {
	json.Marshaler
}

type LatLng interface {
	ConstLatLng
	json.Unmarshaler
}

type latLng struct {
	sync.Mutex
	cachedjson.Cache
	latLngFields
}

type latLngFields struct {
	Lat, Lng float64
}

func NewLatLng(jsonText []byte) (LatLng, error) {
	l := &latLng{}
	u, err := strictjson.NewStructUnmarshaler(&l.latLngFields)
	if err != nil {
		return nil, err
	}
	l.Cache = cachedjson.New(u)
	err = l.UnmarshalJSON(jsonText)
	return l, err
}

func (l *latLng) MarshalJSON() ([]byte, error) {
	l.Lock()
	defer l.Unlock()
	return l.Cache.MarshalJSON()
}

func (l *latLng) UnmarshalJSON(jsonText []byte) error {
	l.Lock()
	defer l.Unlock()
	return l.Cache.UnmarshalJSON(jsonText)
}
