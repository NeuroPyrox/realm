package main

import (
	"crypto/rand"
)

const (
	alphanums       = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	alphanumBits    = 6
	alphanumBitMask = (1 << alphanumBits) - 1
)

type UIDGen interface {
	GenUID() ([]byte, error)
}

type uidGen struct {
	uidSize int
}

func NewUIDGen(uidSize int) UIDGen {
	return uidGen{uidSize}
}

func (ug uidGen) GenUID() ([]byte, error) {
	b := make([]byte, ug.uidSize)
	for i, _ := range b {
		alnum, err := randAlphaNum()
		if err != nil {
			return nil, err
		}
		b[i] = alnum
	}
	return b, nil
}

func randAlphaNum() (byte, error) {
	oneByte := make([]byte, 1)
	for {
		if _, err := rand.Read(oneByte); err != nil {
			return 0, err
		}
		if idx := int(oneByte[0] & alphanumBitMask); idx < len(alphanums) {
			return alphanums[idx], nil
		}
	}
}
