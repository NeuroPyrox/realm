package main

import (
	"log"
	"net/http"
	"time"
)

func main() {
	for {
		srv := http.Server{
			Addr:              ":8080",
			Handler:           NewHandler(NewUIDGen(8)),
			ReadTimeout:       time.Second,
			ReadHeaderTimeout: time.Second,
			WriteTimeout:      time.Second,
			IdleTimeout:       10 * time.Second,
		}
		err := srv.ListenAndServe()
		log.Println(err)
	}
}
