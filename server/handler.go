package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"sync"

	"github.com/gorilla/mux"
)

func requestBody(w http.ResponseWriter, r *http.Request) ([]byte, error) {
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		err = ErrCannotReadRequestBody{err}
		http.Error(w, err.Error(), http.StatusBadRequest)
		return nil, err
	}
	return b, nil
}

type handler struct {
	router *mux.Router
	users  sync.Map
	uidgen UIDGen
}

func NewHandler(uidgen UIDGen) http.Handler {
	h := &handler{
		router: mux.NewRouter(),
		uidgen: uidgen,
	}
	h.router.HandleFunc("/adduser", h.addUser)
	h.router.HandleFunc("/{uid}/remove", h.remove)
	h.router.HandleFunc("/{uid}/setlatlng", h.setLatLng)
	h.router.HandleFunc("/{sender}/message/{receiver}", h.message)
	h.router.HandleFunc("/{uid}/getevents", h.getEvents)
	return h
}

func (h *handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	h.router.ServeHTTP(w, r)
}

func (h *handler) addUser(w http.ResponseWriter, r *http.Request) {
	body, err := requestBody(w, r)
	if err != nil {
		return
	}
	var uidBytes []byte
	var uid string
	var u User
	for {
		uidBytes, err := h.uidgen.GenUID()
		if err != nil {
			http.Error(w,
				fmt.Sprintf("Error generating uid: %v", err),
				http.StatusInternalServerError)
			return
		}
		uid = string(uidBytes)
		u, err = NewUser(uid, body)
		if err != nil {
			http.Error(w,
				fmt.Sprintf("Error creating user: %v", err),
				http.StatusBadRequest)
			return
		}
		_, loaded := h.users.LoadOrStore(uid, u)
		if !loaded {
			break
		}
	}
	h.users.Range(func(key interface{}, value interface{}) bool {
		if key.(string) == uid {
			return true
		}
		other := value.(User)
		other.QueueLoginEvent(LoginEvent{u})
		u.QueueLoginEvent(LoginEvent{other})
		return true
	})
	w.Write(uidBytes)
}

func (h *handler) remove(w http.ResponseWriter, r *http.Request) {
	uid := mux.Vars(r)
	h.users.Delete(uid)
}

func (h *handler) setLatLng(w http.ResponseWriter, r *http.Request) {
	body, err := requestBody(w, r)
	if err != nil {
		return
	}
	uid := mux.Vars(r)["uid"]
	u, err := h.findUser(uid, w)
	if err != nil {
		return
	}
	u.UnmarshalLatLngJSON(body)
}

func (h *handler) message(w http.ResponseWriter, r *http.Request) {
	body, err := requestBody(w, r)
	if err != nil {
		return
	}
	vars := mux.Vars(r)
	receiver, err := h.findUser(vars["receiver"], w)
	if err != nil {
		return
	}
	receiver.QueueMessage(Message{
		Sender: vars["sender"],
		Text:   string(body),
	})
}

func (h *handler) getEvents(w http.ResponseWriter, r *http.Request) {
	uid := mux.Vars(r)["uid"]
	u, err := h.findUser(uid, w)
	if err != nil {
		return
	}
	eventsJSON, err := u.GetEventsJSON()
	if err != nil {
		http.Error(w,
			fmt.Sprintf("Cannot get events json: %v", err),
			http.StatusInternalServerError)
		return
	}
	w.Write(eventsJSON)
}

func (h *handler) findUser(uid string, w http.ResponseWriter) (User, error) {
	val, ok := h.users.Load(uid)
	if !ok {
		err := ErrUserNotFound{uid}
		http.Error(w, err.Error(), http.StatusBadRequest)
		return nil, err
	}
	return val.(User), nil
}

type ErrUserNotFound struct {
	uid string
}

func (err ErrUserNotFound) Error() string {
	return fmt.Sprintf("User not found: %s", err.uid)
}

type ErrCannotReadRequestBody struct {
	err error
}

func (err ErrCannotReadRequestBody) Error() string {
	return fmt.Sprintf("Cannot read request body: %v", err.err)
}
