package main

import (
	"encoding/json"
	"sync"
)

type LoginEvent struct {
	ConstData
}

type LogoutEvent struct {
	UID string
}

type LatLngEvent struct {
	UID    string
	LatLng ConstLatLng
}

type Message struct {
	Sender string
	Text   string
}

type EventStream interface {
	QueueLoginEvent(e LoginEvent)
	QueueLogoutEvent(e LogoutEvent)
	QueueLatLngEvent(e LatLngEvent)
	QueueMessage(m Message)
	GetEventsJSON() ([]byte, error)
}

type eventStream struct {
	sync.Mutex
	events
}

type events struct {
	Logins   []LoginEvent  `json:",omitempty"`
	Logouts  []LogoutEvent `json:",omitempty"`
	LatLngs  []LatLngEvent `json:",omitempty"`
	Messages []Message     `json:",omitempty"`
}

func (es *eventStream) QueueLoginEvent(e LoginEvent) {
	es.Lock()
	defer es.Unlock()
	es.Logins = append(es.Logins, e)
}

func (es *eventStream) QueueLogoutEvent(e LogoutEvent) {
	es.Lock()
	defer es.Unlock()
	es.Logouts = append(es.Logouts, e)
}

func (es *eventStream) QueueLatLngEvent(e LatLngEvent) {
	es.Lock()
	defer es.Unlock()
	es.LatLngs = append(es.LatLngs, e)
}

func (es *eventStream) QueueMessage(e Message) {
	es.Lock()
	defer es.Unlock()
	es.Messages = append(es.Messages, e)
}

func (es *eventStream) GetEventsJSON() ([]byte, error) {
	es.Lock()
	defer es.Unlock()
	b, err := json.Marshal(es.events)
	es.events = events{}
	return b, err
}
